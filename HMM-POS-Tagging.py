"""
==========================================
Hidden Markov Model part-of-speech tagging
==========================================

Author: Haotian Shi, 2016

"""



############################################################
# Hidden Markov Model POS Tagger
############################################################

def load_corpus(path):
    corpus = []
    with open(path) as f:
        for eachLine in f:
            sentence = [tuple(each.split('=')) for each in eachLine.replace('\t', ' ').strip().split(' ') if each != '']
            corpus.append(sentence)
    return corpus

class Tagger(object):

    def __init__(self, sentences):
        self.t = dict()
        self.t_probs = dict()
        self.init_probs = dict()
        self.transition_probs = dict()
        self.emission_probs = dict()
        smoothing = 1e-7
        for sentence in sentences:
            for i in range(len(sentence)):
                if i == 0:
                    if sentence[0][1] not in self.init_probs:
                        self.init_probs[sentence[0][1]] = 1
                    else:
                        self.init_probs[sentence[0][1]] += 1
                else:
                    ti_1 = sentence[i-1][1]
                    ti = sentence[i][1]
                    if ti_1 not in self.transition_probs:
                        self.transition_probs[ti_1] = dict()
                    if ti not in self.transition_probs[ti_1]:
                        self.transition_probs[ti_1][ti] = 1
                    else:
                        self.transition_probs[ti_1][ti] += 1
                ti = sentence[i][1]
                wj = sentence[i][0]
                if ti not in self.emission_probs:
                    self.emission_probs[ti] = dict()
                if wj not in self.emission_probs[ti]:
                    self.emission_probs[ti][wj] = 1
                else:
                    self.emission_probs[ti][wj] += 1
                if wj not in self.t_probs:
                    self.t_probs[wj] = dict()
                if ti not in self.t_probs[wj]:
                    self.t_probs[wj][ti] = 1
                else:
                    self.t_probs[wj][ti] += 1
                if ti not in self.t:
                    self.t[ti] = 1
                else:
                    self.t[ti] += 1

        self.tags = set(self.emission_probs)

        init_sum = len(sentences)
        V_init = len(self.init_probs)
        a_init = smoothing
        for init in self.init_probs:
            self.init_probs[init] = (self.init_probs[init] + a_init) / (init_sum + a_init * (V_init + 1))
        self.init_probs['<UNK>'] = a_init / (init_sum + a_init * (V_init + 1))

        ti_1_sum = 0
        V_ti_1 = len(self.transition_probs)
        a_ti_1 = smoothing
        for ti_1 in self.transition_probs:
            ti_sum = sum(self.transition_probs[ti_1].values())
            V_ti = len(self.transition_probs[ti_1])
            a_ti = smoothing
            for ti in self.transition_probs[ti_1]:
                self.transition_probs[ti_1][ti] = (self.transition_probs[ti_1][ti] + a_ti) / (ti_sum + a_ti * (V_ti + 1))
            self.transition_probs[ti_1]['<UNK>'] = a_ti / (ti_sum + a_ti * (V_ti + 1))
            ti_1_sum += ti_sum
        self.transition_probs['<UNK>'] = dict()
        self.transition_probs['<UNK>']['<UNK>'] = a_ti_1 / (ti_1_sum + a_ti_1 * (V_ti_1 + 1))

        ti_sum = 0
        V_ti = len(self.emission_probs)
        a_ti = smoothing
        for ti in self.emission_probs:
            wj_sum = sum(self.emission_probs[ti].values())
            V_wj = len(self.emission_probs[ti])
            a_wj = smoothing
            for wj in self.emission_probs[ti]:
                self.emission_probs[ti][wj] = (self.emission_probs[ti][wj] + a_wj) / (wj_sum + a_wj * (V_wj + 1))
            self.emission_probs[ti]['<UNK>'] = a_wj / (wj_sum + a_wj * (V_wj + 1))
            ti_sum += wj_sum
        self.emission_probs['<UNK>'] = dict()
        self.emission_probs['<UNK>']['<UNK>'] = a_ti / (ti_sum + a_ti * (V_ti + 1))


    def most_probable_tags(self, tokens):
        tags = []
        for token in tokens:
            if token in self.t_probs:
                most = 0
                for tag in self.t_probs[token]:
                    if self.t_probs[token][tag] > most:
                        most = self.t_probs[token][tag]
                        t = tag
            else: t = 'NOUN'
            '''
                most = 0
                for tag in self.t:
                    if self.t[tag] > most:
                        most = self.t[tag]
                        t = tag
            '''
            tags.append(t)
        return tags

    def viterbi_tags(self, tokens):
        pi = self.init_probs
        a = self.transition_probs
        b = self.emission_probs
        delta = [{tag : 0 for tag in self.tags} for t in range(len(tokens))]#using two deltas to record in turn
        
        for t in range(len(tokens)):
            ot = tokens[t]
            if t == 0:# t==1
                for i in self.tags:
                    if i in pi:
                        if ot in b[i]:
                            delta[0][i] = pi[i] * b[i][ot]
                        else:
                            delta[0][i] = pi[i] * b[i]['<UNK>']
                    else:
                        if ot in b[i]:
                            delta[0][i] = pi['<UNK>'] * b[i][ot]
                        else:
                            delta[0][i] = pi['<UNK>'] * b[i]['<UNK>']
            else:# 2=<t<=T
                for j in self.tags:
                    temp = []
                    for i in self.tags:
                        if i in a:
                            if j in a[i]:
                                temp.append(delta[t-1][i] * a[i][j])
                            else:
                                temp.append(delta[t-1][i] * a[i]['<UNK>'])
                        else:
                            temp.append(delta[t-1][i] * a['<UNK>']['<UNK>'])
                    MAX = max(temp)
                    if ot in b[j]:
                        delta[t][j] = MAX * b[j][ot]
                    else:
                        delta[t][j] = MAX * b[j]['<UNK>']
        tags = []
        end = len(tokens) - 1
        t = end
        while(t >= 0):
            if t == end:
                MAX = 0
                for i in self.tags:
                    if delta[t][i] > MAX:
                        MAX = delta[t][i]
                        tag = i
                tags.append(tag)
                j = tag
            else:
                MAX = 0
                for i in self.tags:
                    if i in a:
                        if j in a[i]:
                            temp = delta[t][i] * a[i][j]
                        else:
                            temp = delta[t][i] * a[i]['<UNK>']
                    else:
                        temp = delta[t][i] * a['<UNK>']['<UNK>']
                    if temp > MAX:
                        MAX = delta[t][i]
                        tag = i
                tags.append(tag)
                j = tag
            t -= 1
        tags.reverse()
        return tags


if __name__ == '__main__':
    c = load_corpus("data\\brown_corpus.txt")
    print c[1402]
    print c[1799]

    t = Tagger(c)
    print t.most_probable_tags(["The", "man", "walks", "."])
    print t.most_probable_tags(["The", "blue", "bird", "sings"])

    s = "I am waiting to reply".split()
    print t.most_probable_tags(s)
    print t.viterbi_tags(s)

    s = "I saw the play".split()
    print t.most_probable_tags(s)
    print t.viterbi_tags(s)

    print t.t

